import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';

Widget buildPasswordField(){

  return TextField(
    obscureText: true,
    
    decoration: 
    
    
    InputDecoration(
      hintText: "Password",
      hintStyle: TextStyle(
        color: Colors.grey,
        fontSize: 20.0,
        fontWeight: FontWeight.normal
      ),
      
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10.0)
      ),
      prefixIcon: Icon(Icons.lock,
      color: Colors.black38),
      suffixIcon: IconButton(
        onPressed: (){},
        icon: Icon(Icons.visibility_off,
        color: Colors.white30,),
      )    ),
      
  );
}
import 'package:flutter/material.dart';

Widget buildEmailField(){
      return TextField(
decoration : InputDecoration(
  hintText: "Email or Username",
  hintStyle: TextStyle(
    color: Colors.grey,
    fontSize: 20.0,
    fontWeight: FontWeight.normal
  ),
  border: OutlineInputBorder(
    borderRadius: BorderRadius.circular(20.0)
    
  ),
  prefixIcon: Icon(Icons.email,
  color: Colors.red,) 
),
      );
    }
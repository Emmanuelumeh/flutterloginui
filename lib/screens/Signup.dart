import 'package:flutter/material.dart';
import 'package:teststuff/widgets/LoginButton.dart';

class Signup extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return SignupState();
  }


}

class SignupState extends State<Signup>{

          
             
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            height : double.infinity,
            width : double.infinity,
            child : Image.asset(
              'images/assets/a739.jpg',
              fit: BoxFit.fill,
            )

          ),

           
    ListView(
      children: <Widget>[
        
   Padding(
     padding: const EdgeInsets.only(top: 20.0),
     child: Column(
      children: <Widget>[
          Container(
                height: 150,
                width: double.infinity,
                child: Center(
                  child: Padding(
                    
                    padding: EdgeInsets.only(top : 0.0,right: 50.0, left: 50.0,bottom: 20.0),
                    child: RichText(
                      text: TextSpan(
                        text: 'Sign Up',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 30.0,
                          fontWeight: FontWeight.bold,
                          
                          letterSpacing: 6.0,
                          height: 1.5
                        )
                      ),
                    )
                    
                  ),
                ),
              ),

        TextField(
decoration : InputDecoration(
  hintText: "Email or Username",
  hintStyle: TextStyle(
      color: Colors.grey,
      fontSize: 20.0,
      fontWeight: FontWeight.normal
  ),
  border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(20.0)
      
  ),
  prefixIcon: Icon(Icons.email,
  color: Colors.red,) 
),
        ),
        SizedBox(height: 10.0,),

        TextField(
decoration : InputDecoration(
  hintText: "Your Name",
  hintStyle: TextStyle(
      color: Colors.grey,
      fontSize: 20.0,
      fontWeight: FontWeight.normal
  ),
  border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(20.0)
      
  ),
  prefixIcon: Icon(Icons.person,
  color: Colors.blue,) 
),
        ),
        SizedBox(height: 10.0,),


        
       TextField(
      obscureText: true,
      
      decoration: 
      
      
      InputDecoration(
        hintText: "Password",
        hintStyle: TextStyle(
          color: Colors.grey,
          fontSize: 20.0,
          fontWeight: FontWeight.normal
        ),
        
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0)
        ),
        prefixIcon: Icon(Icons.lock,
        color: Colors.black38),
        suffixIcon: IconButton(
          onPressed: (){},
          icon: Icon(Icons.visibility_off,
          color: Colors.white30,),
        )    ),
        
  ),
    SizedBox(height: 10.0,),

     TextField(
      obscureText: true,
      
      decoration: 
      
      
      InputDecoration(
        hintText: "Re-Enter Password",
        hintStyle: TextStyle(
          color: Colors.grey,
          fontSize: 20.0,
          fontWeight: FontWeight.normal
        ),
        
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0)
        ),
        prefixIcon: Icon(Icons.lock,
        color: Colors.black38),
        suffixIcon: IconButton(
          onPressed: (){},
          icon: Icon(Icons.visibility_off,
          color: Colors.white30,),
        )  
          ),
        
  ),
   LoginButton(buttonText: Text("Login"),
              textColor: Colors.grey,
              backgroundColor: Theme.of(context).primaryColor,
              onPressed: (){},),


      
        
      ],
  ),
   ),

      ],
    )



        ]


      )
    );
  }


}
import 'package:flutter/material.dart';
import 'package:rect_getter/rect_getter.dart';
import 'package:teststuff/screens/Signup.dart';
import 'package:teststuff/widgets/EmailField.dart';
import 'package:teststuff/widgets/LoginButton.dart';
import 'package:teststuff/widgets/PasswordField.dart';

class HomePage extends StatefulWidget{


  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }

}

class FadeRouteBuilder<T> extends PageRouteBuilder<T> {
  final Widget page;

  FadeRouteBuilder({@required this.page})
      : super(
          pageBuilder: (context, animation1, animation2) => page,
          transitionsBuilder: (context, animation1, animation2, child) {
            return FadeTransition(opacity: animation1, child: child);
          },
        );
}

class HomePageState extends State<HomePage>{
GlobalKey rectGetterKey = RectGetter.createGlobalKey(); //<--Create a key
  Rect rect;                                              //<--Declare field of rect
  
  final Duration animationDuration = Duration(milliseconds: 300);
  final Duration delay = Duration(milliseconds: 300);
  

  void _onTap() async {
    _ripple();
    setState(() => rect = RectGetter.getRectFromKey(rectGetterKey));
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() =>
          rect = rect.inflate(1.3 * MediaQuery.of(context).size.longestSide));
      Future.delayed(animationDuration + delay, _goToNextPage);
    });
  }

  void _goToNextPage() {
    Navigator.of(context)
        .push(FadeRouteBuilder(page: Signup()))
        .then((_) => setState(() => rect = null));
  }

  


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            height : double.infinity,
            width : double.infinity,
            child : Image.asset(
              'images/assets/a739.jpg',
              fit: BoxFit.fill,
            )

          ),

          



        ListView(
          children: <Widget>[
            Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 200,
                width: double.infinity,
                child: Center(
                  child: Padding(
                    
                    padding: EdgeInsets.only(top : 0.0,right: 50.0, left: 50.0,bottom: 20.0),
                    child: RichText(
                      text: TextSpan(
                        text: 'LOGIN',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 30.0,
                          fontWeight: FontWeight.bold,
                          
                          letterSpacing: 6.0,
                          height: 1.5
                        )
                      ),
                    )
                    
                  ),
                ),
              ),

              SizedBox(height: 10.0),
              buildEmailField(),
              SizedBox(height :20.0),
              buildPasswordField(),
              SizedBox(height: 20.0,),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text("Forgotten Password?",
                    
                    style: TextStyle(
                      color: Colors.white
                    ),

                    )
                  ],
                ),
                
              ),
              SizedBox(height: 10.0,),
              LoginButton(buttonText: Text("Login"),
              textColor: Colors.grey,
              backgroundColor: Theme.of(context).primaryColor,
              onPressed: (){},),
              SizedBox(height : 10.0),

              Center(
                child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Row(
                    children: <Widget>[
                      Text("Don't have an account ? ",
                      style: TextStyle(color: Colors.white54,
                      fontWeight: FontWeight.bold),),
                      SizedBox(width: 10.0,),

                      
                     RectGetter(
                       key: rectGetterKey, 
                       child :  FlatButton(
                        child : Text("Sign Up",
                         style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold
                      ),
                      ),
                      onPressed: (){ _onTap();
                      },
                     
                      )
                      )
                      ],
                  )
                ),
              )

              

            ],
          ),

          ],
        )
          
          

          // Column(
          //   children: <Widget>[
          //     TextField(
                
          //     )
          //   ],
          // )
        ],
        
        
      ),
      
      
     
    );
    
    


    
  }





Widget _ripple() {
  GlobalKey rectGetterKey = RectGetter.createGlobalKey(); //<--Create a key
  Rect rect;
  if (rect == null) {
    return Container();
  }
  return Positioned(
    left: rect.left,                                          //<-- Margin from left
    right: MediaQuery.of(context).size.width - rect.right,    //<-- Margin from right
    top: rect.top,                                            //<-- Margin from top
    bottom: MediaQuery.of(context).size.height - rect.bottom, //<-- Margin from bottom
    child: Container(                                         //<-- Blue cirle
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.blue,
      ),
    ),
  );
}

}